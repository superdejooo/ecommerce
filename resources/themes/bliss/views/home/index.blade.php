@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.home.page-title') }}
@endsection

@section('content-wrapper')

    {!! view_render_event('bagisto.shop.home.content.before') !!}
    <a href="https://sale.aliexpress.com/__pc/daily_lifestyle.htm?spm=a2g0o.home.16003.5.650c2c251BGRcx&topTabIds=6273003&gps-id=5948888&scm=1007.19879.116754.0&scm_id=1007.19879.116754.0&scm-url=1007.19879.116754.0&pvid=f45d648b-e512-4c13-92b6-3e9da64958ce&productIds=33003766972" terget="_blank">
    	<img src="{{ env('APP_URL') }}/01-c.jpg" class="img-fluid home-page-banner">
	</a>

{{--    {!! DbView::make(core()->getCurrentChannel())->field('home_page_content')->with(['sliderData' => $sliderData])->render() !!}--}}

    {{ view_render_event('bagisto.shop.home.content.after') }}
@endsection
